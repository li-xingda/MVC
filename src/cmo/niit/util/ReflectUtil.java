package cmo.niit.util;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/*
 *@date2021/1/31 10:37
 *@author李星达
 *
 */
public class ReflectUtil {
    /*
     * 自动读取请求包中参数的值,并自动赋值给指定类型对象的属性
     * 1.如何得到请求参数名
     * 2.读取内容应该赋值给对象中哪一个属性
     * 规则:要求[请求参数的名称]必须与 [当前实体类的属性名相同]
     * */

    public static Object init(Class classFile, HttpServletRequest request) {
        Field fieldArray[] = null;
        Object instance = null;
        //1.获得当前类文件所有的属性信息(相当于得到本次请求所有参数名)
        fieldArray = classFile.getDeclaredFields();
        //2.创建当前实体类的实例对象
        try {
            instance = classFile.newInstance();//用来调用当前类的无参构造方法
            //3.将请求参数内容赋值给对象中同名的属性
            for (Field field : fieldArray) {
                String fieldName = field.getName();//读取当前属性名称,也想当与得到请求参数名
                String typeName = field.getType().getSimpleName();//数据类型
                String value = request.getParameter(fieldName);
                field.setAccessible(true);
                if (value != null && !"".equals(value)) {
                    if (typeName.equals("Integer")) {
                        field.set(instance, Integer.valueOf(value));
                    } else if (typeName.equals("Double")) {
                        field.set(instance, Double.valueOf(value));
                    } else if (typeName.equals("String")) {
                        field.set(instance, value);//instance是一个实体类的实例对象,将value放到同名的属性对象
                    }

                }

            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return instance;
    }

    /*
     *   根据得到的实体类对象的内容自动生成一个insert语句
     *
     * insert into    表名    (字段名,,,,,)          values(值,,,,)
     * -----------    ----   -------------        ---------------
     *       1          2        3                       4
     *
     * 1.实体类的类名即为表名   dept.frm
     * 2. 实体类属性名即为字段名
     * 3.当前对象指定属性的value值
     * */
    public static String createInsert(Object instance) {
        StringBuffer sql = new StringBuffer("insert into ");
        StringBuffer columns = new StringBuffer("(");
        StringBuffer values=new StringBuffer(" values(");
        Class classFile = null;
        String tableName = null;
        Field fieldArray[] = null;
        //1.得到要操作的表文件的名称
        classFile = instance.getClass();
        tableName = classFile.getSimpleName();//得到当前类文件简短名"Emp" "Dept"
        //2.得到需要赋值的字段名
        fieldArray = classFile.getDeclaredFields();
        for (Field field : fieldArray) {//在前面拼  逗号  ,ename
            String fieldName = field.getName();
            field.setAccessible(true);
            Object value= null;
            try {
                value = field.get(instance);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (value!=null&& !"".equals(value)){
                if (!columns.toString().equals("(")){
                    columns.append(",");
                    values.append(",");
                }
                columns.append(fieldName);
                values.append("'");
                values.append(value);//init方法已经将请求数据的值封装到了实体对象中
                values.append("'");
            }
        }
        columns.append(")");
        values.append(")");
        //整理
        sql.append(tableName);
        sql.append(columns);
        sql.append(values);
        System.out.println("INSERT" + sql.toString());
        return sql.toString();
    }

    /*
    * 自动将Result中数据行相关字段内容赋值到指定实体类对象同名属性中
    *
    * */
    public static List getData(ResultSet table,Class classFile){
        ResultSetMetaData rsmd=null;
        List list=new ArrayList();
        Field fieldArray[]=classFile.getDeclaredFields();
        //1.得到临时表字段名称
        try {
            rsmd=table.getMetaData();
            //2.遍历临时表每一行数据并将字段内容读取出来赋值给实体类对象
            while (table.next()){
                Object instance=classFile.newInstance();
                for (int i=1;i<=rsmd.getColumnCount();i++){
                    String columnName=rsmd.getColumnName(i);
                    String value=table.getString(columnName);
                    if (value!=null){
                        for (int j=0;j<fieldArray.length;j++){
                            Field field= fieldArray[j];
                            if (field.getName().equalsIgnoreCase(columnName)){
                                field.setAccessible(true);
                                String typeName=field.getType().getSimpleName();
                                if (typeName.equals("Integer")){
                                    field.set(instance,Integer.valueOf(value));//将null转成基本数据类型造成空指针异常
                                }else if (typeName.equals("Double")){
                                    field.set(instance,Double.valueOf(value));
                                }else if (typeName.equals("String")){
                                    field.set(instance,value);
                                }
                            }
                        }
                    }


                }
                list.add(instance);//一行数据一行数据的放
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
