package cmo.niit.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class JDBCUtil {
    Connection con = null;
    PreparedStatement car =null;
    //-------------------------------------------------------------------
    //方法重载
        public Connection getCon(HttpServletRequest request){
            ServletContext application=request.getServletContext();
            Map map= (ConcurrentHashMap) application.getAttribute("conPool");
            Iterator it= map.keySet().iterator();
            while (it.hasNext()){
                con= (Connection) it.next();//这里不能  Connection  con= (Connection) it.next();这么写,这样属于新创建一个con,导致con=null,不是map里面的con,造成空指针异常
                boolean flag= (boolean) map.get(con);
                if (flag==true){
                    map.put(con,false);
                    return con;
                }

            }
            return null;
        }

        //重载getCar
    public  PreparedStatement getCar(HttpServletRequest request,String sql){
        try {
            car=getCon(request).prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  car;
    }
    //重载close
    public void close(HttpServletRequest request){
            ServletContext application=null;
        if (car!=null){
            try {
                car.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //将使用过得connection设置为空闲状态
        application=request.getServletContext();
        Map map= (ConcurrentHashMap) application.getAttribute("conPool");
        map.put(con,true);
    }
    //-------------------------------------------------------------------

    //简化获得连接通道难度
    public Connection getCon() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bjpowernode?characterEncoding=UTF-8", "root", "root");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
    //获得交通工具
    public PreparedStatement getCar(String sql){

        try {
            car = getCon().prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return car;
    }
    //
    //释放资源
    public void close(){
        if (con!=null){
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (car!=null){
            try {
                car.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    //方法重载
    public void close(ResultSet table){
        if (table!=null){
            try {
                table.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        close();
    }
}