package cmo.niit.serviceImpl;

import cmo.niit.dao.DeptDao;
import cmo.niit.entity.Dept;
import cmo.niit.service.BaseService;
import cmo.niit.util.ReflectUtil;

/*
 *@date2021/2/2 18:49
 *@author李星达
 *
 */
public class DeptService extends BaseService {
    private DeptDao dao=new DeptDao();

    public  String insert(){

        Dept dept= (Dept) ReflectUtil.init(Dept.class,request);
        int flag=dao.insert(dept);
        if (flag==1){
            request.setAttribute("info","部门添加成功");
        }else{
            request.setAttribute("info","部门添加失败");
        }
        return "/info.jsp";
    }
}
