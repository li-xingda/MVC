package cmo.niit.dao;

/*import com.niit.entity.Emp;
import com.niit.util.JDBCUtil;
import com.niit.util.ReflectUtil;*/


import cmo.niit.entity.Emp;
import cmo.niit.util.JDBCUtil;
import cmo.niit.util.ReflectUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EmpDao {
    JDBCUtil util= new JDBCUtil();
    public int login(String userName,String password){
        String sql="select count(*) from emp where ename=? and empno=?";
        ResultSet table=null;
        int flag=0;
        PreparedStatement car= util.getCar(sql);
        try {
            car.setString(1,userName);
            car.setString(2,password);
            table=car.executeQuery();
            table.next();
            flag=table.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(table);
        }
        return flag;
    }

    //insert
    public  int insert(Emp emp){
        String sql= ReflectUtil.createInsert(emp);//static 声明的可以直接这样用? satic声明的可以不用创建实例对象
        PreparedStatement car=util.getCar(sql);
        int flag= 0;
        try {
            flag = car.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close();
        }
        return flag;

    }

    //findAll
    public List<Emp> findAll(){
        String sql="select * from emp";
        ResultSet table=null;
        List list=null;
        PreparedStatement car=util.getCar(sql);
        try {
            table=car.executeQuery();
             list=ReflectUtil.getData(table,Emp.class);//这里List list 可以码
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(table);
        }

        return list;

    }

    //findById
    public Emp findById(String empNo){
        ResultSet table=null;
        String sql="select * from emp where empNo=?";
        List list =null;
        PreparedStatement car=util.getCar(sql);
        try {
            car.setString(1,empNo);
            table=car.executeQuery();
            list=ReflectUtil.getData(table,Emp.class);//这里的list中是每一行的数据
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(table);
        }
        return (Emp) list.get(0);//0表示第一行数据

    }

    //查询领导编号和xingming
    public  List<Emp> findMgr(){
        String sql="select emp.empNo as empNo,emp.ename as ename "+
                "from emp "+
                "join "+
                "(select mgr from emp where mgr is not null  group by mgr) as t1"+
                " on emp.empNo=t1.mgr or emp.mgr is null"+
                " group by emp.empNo";
        PreparedStatement car = util.getCar(sql);
        ResultSet table = null;
        List<Emp> empList=null;

        try {
            table =car.executeQuery();
            empList=ReflectUtil.getData(table,Emp.class);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(table);
        }
        return  empList;
    }
}
