package cmo.niit.dao;

import cmo.niit.entity.Dept;
import cmo.niit.util.JDBCUtil;


import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeptDao {
    //JDBC规范中,连接通道的创建与销毁最耗费时间
    JDBCUtil util =new JDBCUtil();
    public  int insert(Dept dept){
        String sql = "insert into dept (dname,loc) values(?,?)";
        JDBCUtil util = new JDBCUtil();
        PreparedStatement car = util.getCar(sql);

        int flag=0;
        try {
            car.setString(1,dept.getDname());
            car.setString(2,dept.getLoc());
            flag=car.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            util.close();
        }
        return flag;
    }
    //-------------------------------------------------------------
    //重载insert
    public  int insert(Dept dept, HttpServletRequest request){
        String sql = "insert into dept (dname,loc) values(?,?)";
        JDBCUtil util = new JDBCUtil();
        PreparedStatement car = util.getCar(request,sql);

        int flag=0;
        try {
            car.setString(1,dept.getDname());
            car.setString(2,dept.getLoc());
            flag=car.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            util.close(request);
        }
        return flag;
    }
    //--------------------------------------------------------------

    //查询所有部门信息
    public List<Dept> findAll(){
        String sql = "select *from dept";
        PreparedStatement car=util.getCar(sql);
        ResultSet table = null;
        List<Dept> list = new ArrayList();
        try {
            table =car.executeQuery();
            while(table.next()){
                Integer deptNo=table.getInt("deptNo");
                String dname =table.getString("dname");
                String loc=table.getString("loc");
                Dept dept = new Dept(deptNo,dname,loc);//这里把参数封装到实体类
                list.add(dept);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(table);
        }

        return list;
    }

    public int delete(String deptNo){
        String sql="delete from dept where deptNo=?";
        JDBCUtil util = new JDBCUtil();
        PreparedStatement car = util.getCar(sql);

        int flag=0;
        try {
            car.setString(1,deptNo);
            flag=car.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            util.close();
        }
        return flag;

    }

    public Dept findById(String deptNo){
        String sql ="select * from dept where deptNo=?";
        PreparedStatement car=util.getCar(sql);
        Dept dept =null;
        ResultSet table = null;

        try {
            car.setString(1,deptNo);
            table =car.executeQuery();
            while(table.next()){

                String dname =table.getString("dname");
                String loc=table.getString("loc");
                dept = new Dept(Integer.valueOf(deptNo),dname,loc);//这里把参数封装到实体类

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            util.close(table);
        }

        return dept;
    }

    public int update(Dept dept){
        String sql = "update dept set dname=?,loc=? where deptNo=?";
        JDBCUtil util = new JDBCUtil();
        PreparedStatement car = util.getCar(sql);
        int flag=0;

        try {
            car.setString(1,dept.getDname());
            car.setString(2,dept.getLoc());
            car.setInt(3,dept.getDeptNo());
            flag=car.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            util.close();
        }
        return flag;

    }
}
