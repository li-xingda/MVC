package cmo.niit.controller;

import cmo.niit.service.BaseService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;


public class DispatcherServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("DispatcherServlet运行");
        ServletContext application= request.getServletContext();
        String uri=null;
        String serviceName=null;
        String classPath=null;
        Class classFile=null;
        String serviceMethod=null;
        BaseService service=null;
        Method method=null;
        //1.调用请求对象读取 请求行uri
        uri=request.getRequestURI();//   /网站名/service别名.do
        //2.获得被访问的service类的别名
        serviceName= uri.substring(uri.lastIndexOf("/")+1);//从 / 的下一个位置截取
        //3.从全局作用域对象中得到被访问的类
        classPath=application.getInitParameter(serviceName);
        System.out.println(classPath);
        //4.创建service类的实例对象
        try {
            classFile = Class.forName(classPath);//反射机制获得类的管理对象
            service= (BaseService) classFile.newInstance();//反射机制获得类的实例对象
            //5.初始化service对象
            service.request=request;
            //6.根据请求调用service对象中的方法
            serviceMethod=request.getParameter("method");
            method=classFile.getDeclaredMethod(serviceMethod,null);
            String result= (String) method.invoke(service,null);
            request.getRequestDispatcher(result).forward(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
